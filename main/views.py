
from django.shortcuts import render, redirect
from .models import *
from django.views.generic import DetailView
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage


def Index(request):
    new = yangilik.objects.all()
    tech = teacher.objects.all()
    daraja = fikr.objects.all()
    son1 = raqam.objects.all()
    kurs = course.objects.all()
    blog1 = blog.objects.all()
    event1 = event.objects.all()
    context = {
        'news': new,
        'index': "active",
        'techs': tech,
        'key': daraja,
        'key1': son1,
        'key2': kurs,
        'key3': blog1,
        'key4': event1,
    }
    return render(request, 'index.html', context)


def About(request):
    fikr1 = fikr.objects.all()
    ab = about.objects.all()
    son1 = raqam.objects.all()
    context = {
        'about': "active",
        'key1': son1,
        'ab1': ab,
        'fikr1': fikr1,
    }

    return render(request, 'about.html', context)


def Course(request):
    kurs1 = courses.objects.all()
    context = {
        'course': "active",
        'kurs': kurs1,

    }

    return render(request, 'course.html', context)


def Teachers(request):
    teach = teacher.objects.all()
    daraja = fikr.objects.all()
    bitiruvchi = raqam.objects.all()
    context = {
        'teacher': "active",
        'teach': teach,
        'daraja': daraja,
        'bitiruvchi': bitiruvchi
    }
    return render(request, 'teacher.html', context)


def Blog(request):
    galery1 = galery.objects.all()
    context = {

        'blog': "active",
        'galery1': galery1,

    }
    return render(request, 'blog.html', context)


def Event(request):
    event1 = event.objects.all()
    context = {
        'event': "active",
        'event1': event1,
    }
    return render(request, 'event.html', context)


def Contact(request):
    return render(request, 'contact.html', {'contact': "active"})


def Messages(request):

    if request.method == 'POST':
        name = request.POST['name']
        email = request.POST['email']
        subject = request.POST['subject']
        message = request.POST['message']
        m = contact.objects.create(
            name=name, email=email, subject=subject, message=message)
        m.save()
    return redirect('contact')


class TeacherDetail(DetailView):
    template_name = 'teacher-single.html'
    model = teacher
    context_object_name = 'teacher'
    teachers = teacher.objects.all()


def Xabar(request):

    if request.method == 'POST':
        name = request.POST['name']
        email = request.POST['email']
        message = request.POST['msg']
        m = contact1.objects.create(name=name, email=email, message=message)
        m.save()
        print(name)
    return redirect('contact')
