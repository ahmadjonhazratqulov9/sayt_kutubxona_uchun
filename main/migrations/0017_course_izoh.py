# Generated by Django 3.0.8 on 2020-12-03 09:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0016_auto_20201203_1422'),
    ]

    operations = [
        migrations.AddField(
            model_name='course',
            name='izoh',
            field=models.TextField(default=1),
            preserve_default=False,
        ),
    ]
