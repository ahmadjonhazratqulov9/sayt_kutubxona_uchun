# Generated by Django 3.0.8 on 2020-12-03 22:51

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0025_course_kurs_narxi'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='course',
            name='kurs_narxi',
        ),
    ]
