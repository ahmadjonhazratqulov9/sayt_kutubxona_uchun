# Generated by Django 3.0.8 on 2020-12-08 13:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0033_auto_20201208_1826'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contact',
            name='subject',
            field=models.TextField(blank=True, null=True),
        ),
    ]
