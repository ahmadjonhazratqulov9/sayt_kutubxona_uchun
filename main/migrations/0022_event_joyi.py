# Generated by Django 3.0.8 on 2020-12-03 11:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0021_event_vaqt'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='joyi',
            field=models.CharField(default=1, max_length=50),
            preserve_default=False,
        ),
    ]
