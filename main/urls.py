from django.urls import path
from .views import *

urlpatterns = [
    path('', Index, name='index'),
    path('about/', About, name='about'),
    path('course/', Course, name='course'),
    path('teacher/', Teachers, name='teacher'),
    path('blog/', Blog, name="blog"),
    path('event/', Event, name='event'),
    path('contact/', Contact, name='contact'),
    path('messages/', Messages, name='messages'),
    path('xabar/', Xabar, name='contact1'),
    path('teacherdetail/<int:pk>', TeacherDetail.as_view(), name='teacherdetail'),



]
